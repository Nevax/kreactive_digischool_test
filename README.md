# BlablaMovie backend

## Getting started

Install the dependencies:
```bash
composer install
```

Fill up your `.env` file and with your OMDb api key.  
Make sure to generate, migrate the migrations on your own for your database system.

## Endpoints

### Create an user

Route: `/api/users`  
Method: `POST`  

```json
{
    "pseudo": "John Doe",
    "email": "john.doe@email.com",
    "birthDate": "1942-01-01"
}
```

Formats:  
- pseudo: `string`  
- email: `string`  
- birthDate: `yyyy-MM-dd`  

This request will return an 201 status code if it was successfully processed.

```json
{
    "id": 1,
    "pseudo": "John Doe",
    "email": "john.doe@email.com",
    "birth_date": "1942-01-01T00:00:00+00:00",
    "movies": [],
    "created_at": "2019-03-26T20:07:01+00:00"
}
```

Otherwise, it will return 400 status code if the email is not unique.

```json
{
    "code": 400,
    "message": "Validation Failed",
    "errors": {
        "children": {
            "pseudo": {},
            "email": {
                "errors": [
                    "This value is already used."
                ]
            },
            "birthDate": {}
        }
    }
}
```

### Add user favorite movie

Route: `/api/users/{user}/favorites`  
Method: `POST`  

```json
{
  "title": "Interstellar"
}
```

Formats:  
- title (string)

This request will return an 201 status code if it was successfully processed.

```json
{
    "id": 2,
    "title": "interstellar",
    "poster": "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    "users": [
        {
            "pseudo": "Giles",
            "email": "taryn.kiehn@gmail.com",
            "birth_date": "1939-11-16T00:00:00+00:00",
            "movies": [],
            "id": 5,
            "created_at": "2019-03-25T21:37:05+00:00"
        }
    ]
}
```

Otherwise, it will return 400 status code if the movie does not exist or a 422 if the user has already choice this movie.  

```json
{
    "code": 400,
    "message": "Validation Failed",
    "errors": {
        "children": {
            "title": {
                "errors": [
                    "The movie \"1980azefff\" does not exist."
                ]
            }
        }
    }
}
```

```json
{
    "error": {
        "code": 422,
        "message": "Unprocessable Entity"
    }
}
```

### Delete a favorite of a user

Route: `/api/favorites/{favorite}`  
Method: `DELETE`

This request will return a 204 response for success.

Otherwise, if the user does not have this movie in these favorites it will return a 422 UnprocessableEntity.
Or a 404 error if the movie of the user are not found.

### List favorite movies of a user

Route: `/api/users/{user}`  
Method: `GET` 

It will return 200 status code response with the user details (and his favorites movies).
```json
{
    "pseudo": "Hadley",
    "email": "lulu.douglas@christiansen.com",
    "movies": [
        {
            "id": 1,
            "title": "1998",
            "poster": "N/A",
            "users": []
        }
    ],
    "id": 2,
    "created_at": "2019-03-25T21:37:05+00:00"
}
```

Else it will return a 404 error.

### List users who have chosen a movie

Route: `/api/movies/{movie}/`  
Method: `GET`  

It will return 200 status code response with the movie details (and the users who have it in favorites).

```json
{
    "id": 2,
    "title": "interstellar",
    "poster": "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    "users": [
        {
            "pseudo": "Giles",
            "email": "taryn.kiehn@gmail.com",
            "birth_date": "1939-11-16T00:00:00+00:00",
            "movies": [
                {
                    "id": 3,
                    "title": "42",
                    "poster": "https://m.media-amazon.com/images/M/MV5BMTQwMDU4MDI3MV5BMl5BanBnXkFtZTcwMjU1NDgyOQ@@._V1_SX300.jpg",
                    "users": []
                }
            ],
            "id": 5,
            "created_at": "2019-03-25T21:37:05+00:00"
        }
    ]
}
```

### List most favored movies

Route: `/api/favorites`  
Method: `GET`  

Return the ordered list of the most favored movies, each movie has a favoredCount value.

```json
[
    {
        "id": "2",
        "title": "interstellar",
        "poster": "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
        "favoredCount": "2"
    },
    {
        "id": "1",
        "title": "1998",
        "poster": "N/A",
        "favoredCount": "1"
    },
    {
        "id": "3",
        "title": "42",
        "poster": "https://m.media-amazon.com/images/M/MV5BMTQwMDU4MDI3MV5BMl5BanBnXkFtZTcwMjU1NDgyOQ@@._V1_SX300.jpg",
        "favoredCount": "1"
    }
]
```

You can also add a limit with the `limit` query param.

For example :

GET `/api/favorites?limit=1`

```json
[
    {
        "id": 2,
        "title": "interstellar",
        "poster": "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
        "favoredCount": "2"
    }
]
```

## Entities

User:
- id (bigInteger, GeneratedValue)
- pseudo (string)
- email (string)
- birthDate (date)
- movies (ManyToMany relation to Movie)
- createdAt (datetime)

Constraints:  
- `email` is require
- `email` must be unique

Movie:
- id (bigInteger, GeneratedValue)
- title (string)
- poster (string)
- users (ManyToMany relation to User)

Constraints:  
- `title` must be unique

## To do
- Limit the number of favored movies for each users
- Add tests