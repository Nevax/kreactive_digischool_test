<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\DataFixtures;

use App\DataFixtures\Traits\HasFaker;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    use HasFaker;

    public function load(ObjectManager $manager)
    {
        foreach (range(1, 10) as $i) {
            $user = (new User())
                ->setEmail($this->faker->email)
                ->setPseudo($this->faker->firstName)
                ->setBirthDate(rand(0, 1) ? $this->faker->dateTimeThisCentury() : null);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
