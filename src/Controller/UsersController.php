<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Controller;

use App\Controller\Traits\HasEntityManager;
use App\Entity\User;
use App\Form\UserType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UsersController.
 *
 * @Rest\Route("api")
 */
class UsersController extends AbstractFOSRestController
{
    use HasEntityManager;

    /**
     * @Rest\View(StatusCode=Response::HTTP_CREATED)
     * @Rest\Post("/users", name="users")
     *
     * @param Request $request
     *
     * @return User|FormInterface
     */
    public function create(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user)
            ->handleRequest($request)
            ->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $this->getManager()->persist($user);
        $this->getManager()->flush();

        return $user;
    }

    /**
     * @Rest\View(StatusCode=Response::HTTP_OK)
     * @Rest\Get("/users/{id}")
     *
     * @param User $user
     *
     * @return User
     */
    public function show(User $user)
    {
        return $user;
    }
}
