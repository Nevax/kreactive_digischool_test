<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Entity\Traits;

use DateTime;
use DateTimeInterface;
use Exception;

trait HasCreatedAtTimestamp
{
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @throws Exception
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new DateTime();
    }
}
