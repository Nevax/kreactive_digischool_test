<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Controller\Traits;

use Doctrine\Common\Persistence\ObjectManager;

trait HasEntityManager
{
    /**
     * @var ObjectManager
     */
    protected $entityManager;

    public function getManager()
    {
        if (!$this->entityManager instanceof ObjectManager) {
            $this->entityManager = $this->getDoctrine()->getManager();
        }

        return $this->entityManager;
    }
}
