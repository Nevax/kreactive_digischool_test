<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MovieExist extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'The movie "{{ value }}" does not exist.';
}
