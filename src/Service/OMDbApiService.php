<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Service;

use aharen\OMDbAPI;

class OMDbApiService
{
    /**
     * @var OMDbAPI
     */
    private $api;

    /**
     * OMDbApiService constructor.
     *
     * @param string $key injected from service container
     */
    public function __construct(string $key)
    {
        $this->api = new OMDbAPI($key, false, true);
    }

    /**
     * Check if the title movie exist.
     *
     * @param string $title
     *
     * @return bool
     */
    public function checkExist(string $title): bool
    {
        $exist = $this->search($title)['data']['Response'];

        return 'true' === mb_strtolower($exist);
    }

    /**
     * @param string $title
     *
     * @return array
     */
    public function search(string $title): array
    {
        return $result = $this->api->fetch('t', $title);
    }

    /**
     * @param string $title
     *
     * @return string Poster url
     */
    public function getPoster(string $title): string
    {
        return $this->search($title)['data']['Poster'];
    }
}
