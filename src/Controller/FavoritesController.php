<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Controller;

use App\Controller\Traits\HasEntityManager;
use App\Entity\Movie;
use App\Entity\User;
use App\Form\MovieType;
use App\Repository\MovieRepository;
use App\Repository\UserRepository;
use App\Service\OMDbApiService;
use Doctrine\ORM\NonUniqueResultException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class UsersController.
 *
 * @Rest\Route("api")
 */
class FavoritesController extends AbstractFOSRestController
{
    use HasEntityManager;

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(MovieRepository $movieRepository, UserRepository $userRepository)
    {
        $this->movieRepository = $movieRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Rest\View(StatusCode=Response::HTTP_OK)
     * @Rest\Get("/favorites")
     *
     * @param Request $request
     *
     * @return Movie[]|mixed|null
     */
    public function index(Request $request)
    {
        return $this->movieRepository->mostFavored($request->get('limit'));
    }

    /**
     * @Rest\View(StatusCode=Response::HTTP_CREATED)
     * @Rest\Post("/users/{id}/favorites")
     *
     * @param Request        $request
     * @param User           $user
     * @param OMDbApiService $api
     *
     * @throws NonUniqueResultException
     *
     * @return Movie|FormInterface
     */
    public function create(Request $request, User $user, OMDbApiService $api)
    {
        $movie = $this->movieRepository->findOneByTitle($request->get('title'));

        if (!$movie) {
            $movie = new Movie();
            $form = $this->createForm(MovieType::class, $movie)
                ->handleRequest($request)
                ->submit($request->request->all());
            if (!$form->isValid()) {
                return $form;
            }
        } elseif ($movie->getUsers()->contains($user)) {
            throw new UnprocessableEntityHttpException('You already have this movie in your favorites');
        }

        $movie->setPoster($api->getPoster($movie->getTitle()))
            ->addUser($user);
        $this->getManager()->persist($movie);
        $this->getManager()->flush();

        return $movie;
    }

    /**
     * @Rest\View(StatusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/users/{user}/favorites/{movie}")
     *
     * @param User  $user
     * @param Movie $movie
     */
    public function destroy(User $user, Movie $movie)
    {
        if (!$user->getMovies()->contains($movie)) {
            throw new UnprocessableEntityHttpException('The user does not have this movie in these favorites');
        }

        $user->removeMovie($movie);
        $this->getManager()->persist($user);
        $this->getManager()->flush();

        return null;
    }
}
