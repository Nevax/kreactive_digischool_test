<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Validator;

use App\Service\OMDbApiService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class MovieExistValidator extends ConstraintValidator
{
    /**
     * @var OMDbApiService
     */
    private $api;

    public function __construct(OMDbApiService $api)
    {
        $this->api = $api;
    }

    public function validate($title, Constraint $constraint)
    {
        /* @var $constraint MovieExist */
        if (!$constraint instanceof MovieExist) {
            throw new UnexpectedTypeException($constraint, MovieExist::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $title || '' === $title) {
            return;
        }

        // add violation if the title does not exist
        if (!$this->api->checkExist((string) $title)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $title)
                ->addViolation();
        }
    }
}
