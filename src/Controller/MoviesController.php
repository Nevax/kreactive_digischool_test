<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Controller;

use App\Controller\Traits\HasEntityManager;
use App\Entity\Movie;
use App\Service\EasterEggsService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UsersController.
 *
 * @Rest\Route("api")
 */
class MoviesController extends AbstractFOSRestController
{
    use HasEntityManager;

    /**
     * @Rest\View(StatusCode=Response::HTTP_OK)
     * @Rest\Get("/movies/{id}")
     *
     * @param Movie $movie
     *
     * @return Movie
     */
    public function show(Movie $movie)
    {
        return EasterEggsService::magic($movie);
    }
}
