<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Service;

use App\Entity\Movie;

/**
 * Some secret sauce.
 */
class EasterEggsService
{
    const EASTER_KEYWORD = 'star wars';
    const EASTER_URL = 'https://i.kym-cdn.com/entries/icons/original/000/000/157/itsatrap.jpg';

    public static function magic(Movie $movie): Movie
    {
        if (self::containSecret($movie->getTitle())) {
            $movie->setPoster(self::EASTER_URL);
        }

        return $movie;
    }

    private static function containSecret(string $title)
    {
        return false !== mb_strpos(mb_strtolower($title), self::EASTER_KEYWORD);
    }
}
