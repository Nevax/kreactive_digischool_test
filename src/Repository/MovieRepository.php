<?php

/*
 * (c) Antoine GRAVELOT <antoine.gravelot@hotmail.fr>
 */

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    /**
     * @param $value
     *
     * @throws NonUniqueResultException
     *
     * @return Movie|null
     */
    public function findOneByTitle($value): ?Movie
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.title = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param int|null $limit
     *
     * @return Movie[]|mixed|null
     */
    public function mostFavored(int $limit = null)
    {
        return $this->createQueryBuilder('m')
            ->select('m.id, m.title, m.poster, COUNT(m.id) AS favoredCount')
            ->innerJoin('m.users', 'u')
            ->groupBy('m.id')
            ->orderBy('favoredCount', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
